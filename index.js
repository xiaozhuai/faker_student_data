const faker = require('faker/locale/zh_CN');

console.log(`
DROP TABLE IF EXISTS \`student\`;
CREATE TABLE \`student\`
(
    \`id\`     int(0)       NOT NULL AUTO_INCREMENT,
    \`name\`   varchar(255) NOT NULL,
    \`gender\` varchar(255) NOT NULL,
    \`age\`    int(0)       NOT NULL,
    \`grade\`  varchar(255) NOT NULL,
    \`class\`  int(255)     NOT NULL,
    PRIMARY KEY (\`id\`)
);

DROP TABLE IF EXISTS \`score\`;
CREATE TABLE \`score\`
(
    \`student_id\` int(0)       NOT NULL,
    \`subject\`    varchar(255) NOT NULL,
    \`score\`      int(0)       NOT NULL
);
`);

let studentFormat = [
    `'{{name.firstName}}{{name.lastName}}'`,
    `'{{random.arrayElement(["男", "女"])}}'`,
    `{{random.number({"min": 12, "max": 19})}}`,
    `'{{random.arrayElement(["高1", "高2", "高3"])}}'`,
    `{{random.number({"min": 1, "max": 5})}}`
].join(',');

let scoreFormat = [
    '{{random.number({"min": 0, "max": 150})}}'
].join(',');

const count = 2000;

for (let i = 1; i <= count; ++i) {
    console.log(`INSERT INTO \`student\` VALUES (${i},${faker.fake(studentFormat)});`);
    console.log(`INSERT INTO \`score\` VALUES (${i},'语文',${faker.fake(scoreFormat)});`);
    console.log(`INSERT INTO \`score\` VALUES (${i},'数学',${faker.fake(scoreFormat)});`);
    console.log(`INSERT INTO \`score\` VALUES (${i},'英语',${faker.fake(scoreFormat)});`);
}
