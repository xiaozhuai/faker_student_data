# 单表查询

## 统计全校学生数

```mysql
SELECT
	count( * ) AS `count` 
FROM
	`student`;
```
	

## 统计高三 1班学生数

```mysql
SELECT
	count( * ) AS `count` 
FROM
	`student` 
WHERE
	`grade` = '高三' 
	AND `class` = 1;
```


## 统计高三 1班男女生数

```mysql
SELECT
	`gender`,
	count( * ) AS `count` 
FROM
	`student` 
WHERE
	`grade` = '高三' 
	AND `class` = 1 
GROUP BY
	`gender`;
```

## 统计各年级各班的男女生数

```mysql
SELECT
	`grade`,
	`class`,
	`gender`,
	count( * ) AS `count` 
FROM
	`student` 
GROUP BY
	`grade`,
	`class`,
	`gender` 
ORDER BY
	`grade`,
	`class`,
	`gender`;
```


## 统计各年级各班的男女生数 ( 自定义列名 )

```mysql
SELECT
	`grade` AS `年级`,
	`class` AS `班级`,
	`gender` AS `性别`,
	count( * ) AS `人数` 
FROM
	`student` 
GROUP BY
	`grade`,
	`class`,
	`gender` 
ORDER BY
	`grade`,
	`class`,
	`gender`;
```



## 统计高三 1班学生 最大年龄,最小年龄,平均年龄,年龄和,年龄标准差
```mysql
SELECT
	max( `age` ) AS `最大年龄`,
	min( `age` ) AS `最小年龄`,
	avg( `age` ) AS `平均年龄`,
	sum( `age` ) AS `年龄和`,
	std( `age` ) AS `年龄标准差` 
FROM
	`student` 
WHERE
	`grade` = '高三' 
	AND `class` = 1;
```


## 统计所有班级的平均年龄
```mysql
SELECT
	`grade` AS `年级`,
	`class` AS `班级`,
	avg( `age` ) AS `平均年龄` 
FROM
	`student` 
GROUP BY
	`grade`,
	`class`;
```

## 统计平均年龄在15.5以上的班级有哪些

### 方法一 (子查询)

```mysql
SELECT
	* 
FROM
	( 
		SELECT
			`grade` AS `年级`,
			`class` AS `班级`,
			avg( `age` ) AS `平均年龄` 
		FROM
			`student` 
		GROUP BY
			`grade`,
			`class`
	) AS t
WHERE
	`平均年龄` > 15.5;
```

### 方法二 (HAVING子句)
```mysql
SELECT
	`grade` AS `年级`,
	`class` AS `班级`,
	avg( `age` ) AS `平均年龄` 
FROM
	`student` 
GROUP BY
	`grade`,
	`class`
HAVING
	`平均年龄` > 15.5;
```

### 为什么不能写成

```mysql
SELECT
	`grade` AS `年级`,
	`class` AS `班级`,
	avg( `age` ) AS `平均年龄` 
FROM
	`student` 
WHERE
	`平均年龄` > 15.5
GROUP BY
	`grade`,
	`class`;
```

`avg` 函数是一个聚合函数, 聚合函数的执行发生在 `WHERE` 之后, 所以用WHERE会报错, 找不到 `平均年龄这一列` 列

### SQL的执行顺序：
1. 执行FROM
2. WHERE条件过滤
3. GROUP BY分组
4. 执行SELECT投影列 (选择列)
5. HAVING条件过滤
6. 执行ORDER BY排序


# 关联查询

## 将学生信息和各科目分数合并到一张表

### 方法一 (引用两张表 student 和 score)

```mysql
SELECT
	`student`.`name` AS `姓名`,
	`student`.`gender` AS `性别`,
	`student`.`age` AS `年龄`,
	`student`.`grade` AS `年级`,
	`student`.`class` AS `班级`,
	`score`.`subject` AS `科目`,
	`score`.`score` AS `分数` 
FROM
	`student`,
	`score` 
WHERE
	`student`.`id` = `score`.`student_id`;
```

### 方法二 (join)

```mysql
SELECT
	`student`.`name` AS `姓名`,
	`student`.`gender` AS `性别`,
	`student`.`age` AS `年龄`,
	`student`.`grade` AS `年级`,
	`student`.`class` AS `班级`,
	`score`.`subject` AS `科目`,
	`score`.`score` AS `分数` 
FROM
	`student`
	INNER JOIN
		`score`
	ON
		`student`.`id` = `score`.`student_id`;
```

## 将学生信息和总分合并到一张表

### 方法一

```mysql
SELECT
	`student`.`name` AS `姓名`,
	`student`.`gender` AS `性别`,
	`student`.`age` AS `年龄`,
	`student`.`grade` AS `年级`,
	`student`.`class` AS `班级`,
	sum(`score`.`score`) AS `总分` 
FROM
	`student`,
	`score` 
WHERE
	`student`.`id` = `score`.`student_id`
GROUP BY 
	`score`.`student_id`;
```

### 方法二

```mysql
SELECT
	`student`.`name` AS `姓名`,
	`student`.`gender` AS `性别`,
	`student`.`age` AS `年龄`,
	`student`.`grade` AS `年级`,
	`student`.`class` AS `班级`,
	t.`total_score` AS `总分`
FROM
	`student`
	INNER JOIN
		(
			SELECT
				student_id,
				sum(`score`) as total_score
			FROM 
				`score`
			GROUP BY 
				`student_id`
		) AS t
	ON 
		`student`.`id` = t.`student_id`;
```

## 将学生信息,各科分数,总分 合并到一张表

### 方法一 (较慢)

```mysql
SELECT
	`student`.`name` AS `姓名`,
	`student`.`gender` AS `性别`,
	`student`.`age` AS `年龄`,
	`student`.`grade` AS `年级`,
	`student`.`class` AS `班级`,
	t0.`total_score` AS `总分`,
	t1.`score` AS `语文`,
	t2.`score` AS `数学`,
	t3.`score` AS `英语`
FROM
	`student`
	INNER JOIN
		(
			SELECT
				`student_id`,
				sum(`score`) as total_score
			FROM 
				`score`
			GROUP BY 
				`student_id`
		) AS t0
	ON 
		`student`.`id` = t0.`student_id`
	INNER JOIN 
		(
			SELECT
				`student_id`,
				`score`
			FROM 
				`score`
			WHERE 
				`subject` = '语文'
		) AS t1
	ON 
		`student`.`id` = t1.`student_id`
	INNER JOIN 
		(
			SELECT
				`student_id`,
				`score`
			FROM 
				`score`
			WHERE 
				`subject` = '数学'
		) AS t2
	ON 
		`student`.`id` = t2.`student_id`
	INNER JOIN 
		(
			SELECT
				`student_id`,
				`score`
			FROM 
				`score`
			WHERE 
				`subject` = '英语'
		) AS t3
	ON 
		`student`.`id` = t3.`student_id`;
```

### 方法二 (最快)

```mysql
SELECT
	`student`.`name` AS `姓名`,
	`student`.`gender` AS `性别`,
	`student`.`age` AS `年龄`,
	`student`.`grade` AS `年级`,
	`student`.`class` AS `班级`,
	sum( `score`.`score` ) AS `总分`,
	max( 
		CASE `score`.`subject` 
			WHEN '语文' THEN `score`.`score` 
			ELSE 0 
		END
	) AS `语文`,
	max( 
		CASE `score`.`subject`
			WHEN '数学' THEN `score`.`score`
			ELSE 0 
		END
	) AS `数学`,
	max(
		CASE `score`.`subject`
			WHEN '英语' THEN `score`.`score`
			ELSE 0
		END
	) AS `英语`
FROM
	`student`,
	`score` 
WHERE
	`student`.`id` = `score`.`student_id` 
GROUP BY
	`student`.`id`;
```

### 方法三 (最慢)

```mysql
SELECT
	`student`.`name` AS `姓名`,
	`student`.`gender` AS `性别`,
	`student`.`age` AS `年龄`,
	`student`.`grade` AS `年级`,
	`student`.`class` AS `班级`,
	(
		SELECT 
			sum( `score`.`score` ) 
		FROM 
			`score`
		WHERE 
			`student`.`id` = `score`.`student_id` 
		GROUP BY 
			`score`.`student_id`
	) AS `总分`,
	(
		SELECT
			`score`.`score` 
		FROM 
			`score`
		WHERE 
			`student`.`id` = `score`.`student_id` 
			AND `score`.`subject` = '语文'
	) AS `语文`,
	(
		SELECT
			`score`.`score` 
		FROM
			`score` 
		WHERE 
			`student`.`id` = `score`.`student_id`
			AND `score`.`subject` = '数学'
	) AS `数学`,
	(
		SELECT
			`score`.`score`
		FROM
			`score`
		WHERE
			`student`.`id` = `score`.`student_id`
			AND `score`.`subject` = '英语'
	) AS `英语`
FROM
	`student`;
```

## 统计所有班级各科目的 平均分, 最高分

### 方法一

```mysql
SELECT
	`student`.`grade` AS `年级`,
	`student`.`class` AS `班级`,
	`score`.`subject` AS `科目`,
	avg( `score`.`score` ) AS `平均分`,
	max( `score`.`score` ) AS `最高分` 
FROM
	`student`,
     `score`
WHERE
    `student`.`id` = `score`.`student_id`
GROUP BY
	`student`.`grade`,
	`student`.`class`,
	`score`.`subject`;
```

### 方法二

```mysql
SELECT
	`student`.`grade` AS `年级`,
	`student`.`class` AS `班级`,
	`score`.`subject` AS `科目`,
	avg( `score`.`score` ) AS `平均分`,
	max( `score`.`score` ) AS `最高分` 
FROM
	`student`
	INNER JOIN
		`score`
	ON
		`student`.`id` = `score`.`student_id` 
GROUP BY
	`student`.`grade`,
	`student`.`class`,
	`score`.`subject`;
```

## 统计所有班级 各科目的平均分, 各科目最高分, 总分平均分, 总分最高分 (一列显示)

### 方法一 (子查询)

```mysql
SELECT
    `年级`,
    `班级`,
    avg( `总分` ) as `总分平均分`,
    avg( `语文` ) as `语文平均分`,
    avg( `数学` ) as `数学平均分`,
    avg( `英语` ) as `英语平均分`,
    max( `总分` ) as `总分最高分`,
    max( `语文` ) as `语文最高分`,
    max( `数学` ) as `数学最高分`,
    max( `英语` ) as `英语最高分`
FROM
    (
        SELECT
        	#`student`.`name` AS `姓名`,
        	#`student`.`gender` AS `性别`,
        	#`student`.`age` AS `年龄`,
        	`student`.`grade` AS `年级`,
        	`student`.`class` AS `班级`,
        	sum( `score`.`score` ) AS `总分`,
        	max( 
        		CASE `score`.`subject` 
        			WHEN '语文' THEN `score`.`score` 
        			ELSE 0 
        		END
        	) AS `语文`,
        	max( 
        		CASE `score`.`subject`
        			WHEN '数学' THEN `score`.`score`
        			ELSE 0 
        		END
        	) AS `数学`,
        	max(
        		CASE `score`.`subject`
        			WHEN '英语' THEN `score`.`score`
        			ELSE 0
        		END
        	) AS `英语`
        FROM
        	`student`,
        	`score` 
        WHERE
        	`student`.`id` = `score`.`student_id` 
        GROUP BY
        	`student`.`id`
    ) AS t
GROUP BY 
    `年级`,
    `班级`;
```
